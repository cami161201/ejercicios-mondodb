db.getCollection("Productos").insertMany([{
  "nombreProducto":"Ola Detergente Poder Activo Higiene Total 3.8kg",
  "precio":66,
  "descripcion":"La combinaci�n de ingredientes y acci�n de las enzimas garantizan una limpieza profunda de manchas, entre las mas comunes: aceite de cocina, pasto, chocolate, y sangre.Un tiempo previo de remojo ayuda a obtener mejores resultados",
  "fechaIngreso": new Date()
},
{
 "nombreProducto":"Ola Aromatic Limpiador Desinfectante para Pisos Brisa Marina 5L ",
 "precio":48,
  "descripcion":"Siente la pureza y la frescura del mar con un toque c�trico.El nuevo ola aromatic fusi�n, tiene una exclusiva f�rmula que limpia eficazmente todo tipo de superficies lavables al agua, otrorgando brillo y desinfecta eliminando virus y el 99.9% de bacterias que causan enfermedades",
  "fechaIngreso": new Date()
},
{
  "nombreProducto":"Campos de Solana Cl�sico Blanco 700ml",
  "precio":21,
  "descripcion":"Vino blanco de corte elaborado con las mejores uvas que pueden encontrarse en los valles de altura tarije�os.Campos de Solana concibe este vino a partir de uvas provenientes de los valles centrales de Tarija, ubicados a 2000 m.s.n.m., en una zona que goza de alta luminosidad y pureza de ambiente.",
  "fechaIngreso": new Date()
},
{
 "nombreProducto":"Trix Cereal 480gr",
 "precio":36,
  "descripcion":"Si lo que te gusta desayunar son frutitas, esta es tu opci�n. Cereal Trix de Nestl� con sabor a fruta y adem�s fortificado con 5 vitaminas y 3 minerales esenciales.",
  "fechaIngreso": new Date()
},
{
 "nombreProducto":"Nescaf� Original 100gr",
 "precio":30,
  "descripcion":"Un aut�ntico caf� 100% puro, con sabor extra-fuerte, perfecto para vos que apreci�s un caf� con sabor marcado.Elaborado con una cuidadosa selecci�n de granos tostados para lograr su caracter�stico sabor",
  "fechaIngreso": new Date()
},
])
/*elestudiante debera ingresar minimo 5 registros, tambien debera realizar una actualizacion en un documento*/
db.getCollection("Productos").updateOne({"_id":ObjectId("62b3db91cd53abf28cb37791")},{$set:{"descripcion":"Ahora nada"}})
/*realizar una consulta para cada coleccion*/
db.getCollection("Productos").find({})
