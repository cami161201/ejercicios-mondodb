db.getCollection("usuario").insertMany([{
  "Nombre": "Fatima",
  "Apellido": "Luizaga",
  "Correo": "fatluiz111@gmail.com",
  "Contraseņa": "naranja1789",
  "Direccion": "Av.Blanco Galindo",
  "TarjetaDebito": "4400 1234 2667 3489",
  "Fecharegistro": new Date()
},
{
  "Nombre": "Lorena",
  "Apellido": "Mamani",
  "Correo": "lorenmamani223@gmail.com",
  "Contraseņa": "ssssd3",
  "Direccion": "Av.Circunvalacion",
  "TarjetaDebito": "5656 6789 8905 3467"
  , "Fecharegistro": new Date()
},
{
  "Nombre": "Marcelo",
  "Apellido": "Camacho",
  "Correo": "marcegok344@gmail.com",
  "Contraseņa": "334k5", "Direccion": "Av.Petrolera",
  "TarjetaDebito": "8233 6789 8905 3890",
  "Fecharegistro": new Date()
},
{
  "Nombre": "Juan",
  "Apellido": "Navarro",
  "Correo": "navarjuan59@gmail.com",
  "Contraseņa": "57b9jc",
  "Direccion": "Av.6 de agosto",
  "TarjetaDebito": "3456 6709 2105 3467"
  , "Fecharegistro": new Date()
},
{
  "Nombre": "Julio Cesar",
  "Apellido": "Lopez Obrador",
  "Correo": "cesarjualio59@gmail.com",
  "Contraseņa": "57b9jc",
  "Direccion": "Av.6 de agosto",
  "TarjetaDebito": "1234 6489 8094 5760"
  ,"Fecharegistro": new Date()
}

])

var idFatima = ObjectId("62b26d5bcd53abf28cb3771a")
var idLorena = ObjectId("62b26d5bcd53abf28cb3771b")
var idMarcelo = ObjectId("62b26d5bcd53abf28cb3771c")
var idJuan = ObjectId("62b26d5bcd53abf28cb3771d")
var idJulioCesar = ObjectId("62b26d5bcd53abf28cb3771e")

db.getCollection("registro").insertMany([{
  "Nombre": "Fatima",
  "Apellido": "Luizaga",
  "Correo": "fatluiz111@gmail.com",
  "estadoCuenta":"activo",
  "Fecharegistro": new Date(),
  "idUsuario": idFatima
},
{
  "Nombre": "Lorena",
  "Apellido": "Mamani",
  "Correo": "lorenmamani223@gmail.com",
  "estadoCuenta":"activo",
   "Fecharegistro": new Date(),
   "idUsuario": idLorena
},
{
  "Nombre": "Marcelo",
  "Apellido": "Camacho",
  "Correo": "marcegok344@gmail.com",
  "estadoCuenta":"activo",
  "Fecharegistro": new Date(),
  "idUsuario": idMarcelo
},
{
  "Nombre": "Juan",
  "Apellido": "Navarro",
  "Correo": "navarjuan59@gmail.com",
  "estadoCuenta":"activo",
  "Fecharegistro": new Date(),
  "idUsuario": idJuan
},
{
  "Nombre": "Julio Cesar",
  "Apellido": "Lopez Obrador",
  "Correo": "navarjuan59@gmail.com",
  "estadoCuenta":"activo",
  "Fecharegistro": new Date(),
  "idUsuario": idJulioCesar
}
])

var idFatima = ObjectId("62b26d5bcd53abf28cb3771a")
var idLorena = ObjectId("62b26d5bcd53abf28cb3771b")
var idMarcelo = ObjectId("62b26d5bcd53abf28cb3771c")
var idJuan = ObjectId("62b26d5bcd53abf28cb3771d")
var idJulioCesar = ObjectId("62b26d5bcd53abf28cb3771e")

db.getCollection("compra").insertMany([{
  "articulosComprados":["Detergente","Cereal","CocaCola","Leche","Carne"],
  "cantidad":5,
  "monto":56,
  "fechaCompra": new Date(),
  "idUsuario": idFatima
},
{
 "articulosComprados":["Vino","Papas fritas"],
 "cantidad":2,
 "monto": 40,
 "fechaCompra": new Date(),
   "idUsuario": idLorena
},
{
 "articulosComprados":["Paņales","Cerveza","Carne","Leche"],
 "cantidad":4,
 "monto":74 ,
 "fechaCompra": new Date(),
  "idUsuario": idMarcelo
},
{
 "articulosComprados":["Agua","Papel higienico"],
 "cantidad":2,
 "monto":30,
 "fechaCompra": new Date(),
  "idUsuario": idJuan
},
{
  "articulosComprados":["Rosas","Peluche","Pasta","Fideo"],
  "cantidad":4,
  "monto":102,
  "fechaCompra": new Date(),
  "idUsuario": idJulioCesar
}
])
/*Se debe aplicar indexacion para mayor fluidez al manejo de datos*/
db.getCollection("usuario").createIndex({"Apellido":1})
db.getCollection("usuario").createIndex({"TarjetaDebito":1})
db.getCollection("usuario").getIndexes()

db.getCollection("registro").createIndex({"Apellido":1})
db.getCollection("registro").createIndex({"estadoCuenta":1})
db.getCollection("registro").getIndexes()

db.getCollection("compra").createIndex({"fechaCompra":1})
db.getCollection("compra").getIndexes()

/*Realizar una consulta para cada coleccion*/
db.getCollection("usuario").find({})
db.getCollection("registro").find({})
db.getCollection("compra").find({})

/*Verificar que un usuario realizo una compra*/
db.getCollection("compra").find({})
db.getCollection("usuario").find({})
var idJulioCesar = ObjectId("62b26d5bcd53abf28cb3771e")
db.getCollection("compra").find({"idUsuario":idJulioCesar})

/*El correo electronico no debe repetirse*/
db.getCollection("usuario").createIndex({"Correo":1},{unique:true})
db.getCollection("usuario").getIndexes()
