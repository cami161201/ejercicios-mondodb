db.getCollection("registroRenta").insertMany([
  {
    "nombreCliente": "Jose Luis",
    "nombrePelicula": "Bucando a Nemo",
    " fecha_renta": new Date(),
  },
  {
    "nombreCliente": "Luisa",
    "nombrePelicula": "Barbie y el castillo de Diamantes",
    " fecha_renta": new Date(2022,3,8),
  },
  {
    "nombreCliente": "Fernando",
    "nombrePelicula": "Iron Man",
    " fecha_renta": new Date(),
  },
  {
    "nombreCliente": "Marcela",
    "nombrePelicula": "El resplandor",
    " fecha_renta": new Date(),
  },
  {
    "nombreCliente": "Pamela Sofia",
    "nombrePelicula": "Capit�n Am�rica: El primer vengador",
    " fecha_renta": new Date(),
  },
  {
    "nombreCliente": "Abigail",
    "nombrePelicula": "Pinocho",
    " fecha_renta": new Date(2021,12,5),
  },
  {
    "nombreCliente": "Pepe",
    "nombrePelicula": "Blancanieves y los siete enanitos",
    " fecha_renta": new Date(),
  },
  {
    "nombreCliente": "Jazmin",
    "nombrePelicula": "Expediente Warren",
    " fecha_renta": new Date(2022,1,4),
  },
])

db.getCollection("registroRenta").count()
db.getCollection("registroRenta").find({})
db.getCollection("registroRenta").createIndex({"nombreCliente":1})
db.getCollection("registroRenta").getIndexes()