db.getCollection("Familia").insertMany([
  {
    "nombre":"Luisa Garcia",
    "rolFamilia":"Madre",
    "edad":40
  },
  {
    "nombre":"Esteban Arce",
    "rolFamilia":"Padre",
    "edad":41
  },
  {
    "nombre":"Jose Arce Garcia",
    "rolFamilia":"hijo",
    "edad":10
  },
  {
    "nombre":"Pedro Arce Garcia",
    "rolFamilia":"hijo",
    "edad":6
  },
  {
    "nombre":"Lucia Arce Garcia",
    "rolFamilia":"hija",
    "edad":2
  }
  ])
  
  /*Para evitar este tiempo de demora, se puede indicar que el �ndice se 
  realice en background o como operaci�n en segundo plano:
    db.articulos.ensureIndex({"nombre":1, "fecha":-1}, {background: true})*/
  
  db.Familia.ensureIndex({"nombre":1}, {background: true})
  
  db.getCollection("Familia").getIndexes()