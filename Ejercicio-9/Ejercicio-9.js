db.getCollection("registroCompra").insertMany([
  {
    "nombreComprador": "Jose Luis Perez",
    "pago": "efectivo",
    "articulosComprados": ["Detergente", "Vino", "CocaCola"],
    "precioArticulos": [23, 45, 13],
    "numeroArticulos": 3,
    "monto": 81
  },
  {
    "nombreComprador": "Daniela Lopez",
    "pago": "tarjeta",
    "articulosComprados": ["Papas fritas", "Nachos", "Cafe", "Guantes"],
    "precioArticulos": [13, 12, 15, 20],
    "numeroArticulos": 4,
    "monto": 60
  },
  {
    "nombreComprador": "Lucas del Prado Rivera",
    "pago": "efectivo",
    "articulosComprados": ["Agua", "Dulces"],
    "precioArticulos": [10, 6],
    "numeroArticulos": 2,
    "monto": 16
  },
  {
    "nombreComprador": "Alberto Rodriguez",
    "pago": "efectivo",
    "articulosComprados": ["Arroz","Carne de res","Sal"],
    "precioArticulos": [12,19,5],
    "numeroArticulos": 3,
    "monto": 36
  },
  {
    "nombreComprador": "Luciana Pereira ",
    "pago": "tarjeta",
    "articulosComprados": ["Shampoo","Acondicionador","Jabon","Pasta dental"],
    "precioArticulos": [45,47,18,12],
    "numeroArticulos": 4,
    "monto": 122
  },
  {
    "nombreComprador": "Pamela Pokes Santivaņez",
    "pago": "tarjeta",
    "articulosComprados": ["Televisor 32 pulgadas","Tablet"],
    "precioArticulos": [230,270],
    "numeroArticulos": 2,
    "monto": 500
  },
    {
    "nombreComprador": "Erika Nieto",
    "pago": "efectivo",
    "articulosComprados": ["Cafe"],
    "precioArticulos": [23],
    "numeroArticulos": 1,
    "monto": 23
  }
])
  
 db.getCollection("registroCompra").count()
db.getCollection("registroCompra").find({"pago":"efectivo"})
db.getCollection("registroCompra").find({"numeroArticulos":{$gt:1}})