db.getCollection("alumno").insertMany([
  {
    "nombres": "Luciana",
    "apellidos": "Duran Rocha",
    "edad": 23,
    "sexo": "Femenino",
    "fechaNacimiento": new Date(1999, 2, 12),
    "ci": 9443513,
    "incapacidad": "Ninguna",
    "numeroTelefono": 63300283
  },
  {
    "nombres": "Lucia",
    "apellidos": "Ramirez",
    "edad": 18,
    "sexo": "Femenino",
    "fechaNacimiento": new Date(2002, 3, 12),
    "ci": 90404358,
    "incapacidad": "respiratoria",
    "numeroTelefono": 68330243
  },
  {
    "nombres": "Jose Kevin",
    "apellidos": "Saavedra Acha",
    "edad": 19,
    "sexo": "Masculino",
    "fechaNacimiento": new Date(2003, 6, 24),
    "ci": 1347836,
    "incapacidad": "auditiva",
    "numeroTelefono": 63437819
  },
  {
    "nombres": "Felipe",
    "apellidos": "Luizaga Calani",
    "edad": 19,
    "sexo": "Masculino",
    "fechaNacimiento": new Date(2004, 6, 24),
    "ci": 9008432,
    "incapacidad": "Ninguna",
    "numeroTelefono": 63832633
  },
  {
    "nombres": "Alejandra",
    "apellidos": "Flores Campos",
    "edad": 22,
    "sexo": "Femenino",
    "fechaNacimiento": new Date(2000, 4, 16),
    "ci": 96830983,
    "incapacidad": "respiratoria",
    "numeroTelefono": 60134873
  }
])



db.getCollection("maestro").insertMany([{
  "nombres": "Jose Luis",
  "apellidos": "Selva Alarcon",
  "edad": 44,
  "sexo": "Masculino",
  "fechaNacimiento": new Date(1978, 8, 30),
  "ci": 6383436,
  "DUI": 63124789,
  "incapacidad": "Ninguna",
  "numeroTelefono": 63321436
},
{
  "nombres": "Maria del pilar",
  "apellidos": "Zambrana Ruiz",
  "edad": 31,
  "sexo": "Femenino",
  "fechaNacimiento": new Date(1990, 12, 16),
  "ci": 8346137,
  "DUI": 93686243,
  "incapacidad": "Ninguna",
  "numeroTelefono": 73443873
},



{
  "nombres": "Felicia",
  "apellidos": "Via Orozco",
  "edad": 24,
  "sexo": "Masculino",
  "fechaNacimiento": new Date(1998, 11, 20),
  "ci": 1347036,
  "DUI": 60334378,
  "incapacidad": "auditiva",
  "numeroTelefono": 60438019
},



{
  "nombres": "Silvio",
  "apellidos": "Luizaga Calani",
  "edad": 33,
  "sexo": "Masculino",
  "fechaNacimiento": new Date(1987, 6, 24),
  "ci": 9062132,
  "DUI": 42738634,
  "incapacidad": "Ninguna",
  "numeroTelefono": 63832633
},



{
  "nombres": "Rocio",
  "apellidos": "Flores Campos",
  "edad": 40,
  "sexo": "Femenino",
  "fechaNacimiento": new Date(1980, 4, 16),
  "ci": 9609423,
  "DUI": 3670802,
  "incapacidad": "habla",
  "numeroTelefono": 72334809
}

])

//indexacion de alumnos
db.getCollection("alumno").createIndex({ "apellidos": 1 })
db.getCollection("alumno").createIndex({ "incapacidad": 1 })
db.getCollection("alumno").createIndex({ "ci": 1 })
db.getCollection("alumno").getIndexes()


//indexacion de maestros
db.getCollection("maestro").createIndex({ "apellidos": 1 })
db.getCollection("maestro").createIndex({ "incapacidad": 1 })
db.getCollection("maestro").createIndex({ "DUI": 1 })
db.getCollection("maestro").getIndexes()



/*Realizar una consulta a cada coleccion */
db.getCollection("alumno").find({})
db.getCollection("maestro").find({})


