db.getCollection("registroPaciente").insertMany([{
  "nombre": "Jose Luis Ponce",
  "edad": 26,
  "genero": "Masculino",
  "estado": "Ingresado",
  "estadoCivil": "Soltero",
  "direccion": "Av.Blanco Galindo"
},
{
  "nombre": "Fernanda Luizaga",
  "edad": 45,
  "genero": "Femenino",
  "estado": "Ingresado",
  "estadoCivil": "Casada",
  "direccion": "Av.6 de Agosto"
},
{
  "nombre": "Leandro Perez",
  "edad": 10,
  "genero": "Masculino",
  "estado": "Salido",
  "estadoCivil": "Soltero",
  "direccion": "Av.16 de julio"
},
{
  "nombre": "Julia Paredes",
  "edad": 55,
  "genero": "Femenino",
  "estado": "Ingresado",
  "estadoCivil": "Divorciada",
  "direccion": "Av.Petrolera"
},
{
  "nombre": "Pedro Alcantara",
  "edad": 67,
  "genero": "Masculino",
  "estado": "Salido",
  "estadoCivil": "Viudo",
  "direccion": "Av.Jose Ballivian"
},
])
/*El estudiante debera hacer una consulta con los pacientes que en su campo estado aparezcan ingresados*/
db.getCollection("registroPaciente").find({"estado":"Ingresado"})
/*El estudiante mostrara todos los datos generales de la coleccion*/
db.getCollection("registroPaciente").find({})
db.getCollection("registroPaciente").find({"genero":"Masculino"})
db.getCollection("registroPaciente").find({"genero":"Femenino"})
db.getCollection("registroPaciente").find({"estadoCivil":"Soltero"})
db.getCollection("registroPaciente").find({"estadoCivil":"Casada"})