db.getCollection("Cliente").insertMany([
  {
    "nombre": "Jose Luis",
    "apellido": "Pelaez",
    "edad": 23,
    "correo": "pelaezjose@gmail.com"
  },
  {
    "nombre": "Luisa",
    "apellido": "Lopez Caceres",
    "edad": 33,
    "correo": "luisa122@gmail.com"
  },
  {
    "nombre": "Fernando",
    "apellido": "Lascurain",
    "edad": 45,
    "correo": "lascuraian23@gmail.com"
  },
  {
    "nombre": "Marcela",
    "apellido": "Ya�ez Pereira",
    "edad": 23,
    "correo": "pereira556@gmail.com"
  },
  {
    "nombre": "Pamela Sofia",
    "apellido": "Pokes Santiva�ez",
    "edad": 33,
    "correo": "pokespamela223@gmail.com"

  },
  {
    "nombre": "Abigail",
    "apellido": "Calani Vela",
    "edad": 35,
    "correo": "anivela90@gmail.com"
  },
  {
    "nombre": "Pepe",
    "apellido": "Meneces",
    "edad": 55,
    "correo": "algurpepe@gmail.com"
  },
])
db.getCollection("Cliente").count()
db.getCollection("Cliente").find({"edad":23})
db.getCollection("Cliente").find({"edad":33})
db.getCollection('Cliente').find({})