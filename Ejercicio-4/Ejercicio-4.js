db.getCollection("dueno").insertMany([
  {
    "nombreDueno": "Luisa Fernandez",
    "edadDueno": 26,
    "nombreMascota": "Cuqui"
  },
  {
    "nombreDueno": "Julio Paredes",
    "edadDueno": 15,
    "nombreMascota": "Fido"
  },
  {
    "nombreDueno": "Maria del Pilar Calani",
    "edadDueno": 35,
    "nombreMascota": "ShaSha"
  },
  {
    "nombreDueno": "Miguel Lopez",
    "edadDueno": 65,
    "nombreMascota": "Luffy"
  },
  {
    "nombreDueno": "Fausta Rivera",
    "edadDueno": 23,
    "nombreMascota": "Linda"
  }
])



db.getCollection("mascota").insertMany([
  {
    "nombreMascota": "Cuqui",
    "edadMascota": 12,
    "nombreDueno": "Luisa Fernandez"
  },
  {
    "nombreMascota": "Fido",
    "edadMascota": 5,
    "nombreDueno": "Julio Paredes"
  },
  {
    "nombreMascota": "ShaSha",
    "edadMascota": 1,
    "nombreDueno": "Maria del Pilar Calani"
  },
  {
    "nombreMascota": "Luffy",
    "edadMascota": 8,
    "nombreDueno": "Miguel Lopez"
  },
  {
    "nombreMascota": "Linda",
    "edadMascota": 10,
    "nombreDueno": "Fausta Rivera"
  }])


/* Aplicar indexacion */
db.getCollection("dueno").createIndex({ "nombreDueno": 1 })
db.getCollection("dueno").createIndex({ "nombreMascota": 1 })
db.getCollection("dueno").getIndexes()

/*Realizar una consulta para cada coleccion */
db.getCollection("dueno").find({})
db.getCollection("mascota").find({})























