db.getCollection("movies").insertMany([
  {
    "titulo": "Algun dia",
    "escritor": "Quetin Tarantino",
    "actores": ["Megan Fox","Jennifer Aniston"],
    "franquicia": "Cualquiera",
    "sipnosis": "Lorem tecto Gandalf molestias modi ullam blanditiis ab!",
    "anio": 1990
  },
  {
    "titulo": "Un buen dia",
    "escritor": "Benson Smith",
    "actores": ["Will Smith","Scarlett Johansson"],
    "franquicia": "The Hobbit",
    "sipnosis": "Lorem gold ipsum, Bildo",
    "anio": 1995
  },
  {
    "titulo": "Porque no",
    "escritor": "Julio Prado",
    "actores": ["Brad Pitt","Julene Renee"],
    "franquicia": "Warner Brothers",
    "sipnosis": "No se que poner por eso pongo cualquier cosa Gandalf",
    "anio": 2001
  },
  {
    "titulo": "Avatar",
    "escritor": "Qiensera",
    "actores": ["Zoe Salda�a","Giovanni Ribisi","Matt Gerald"],
    "franquicia": "Avatar Studios",
    "sipnosis": "Jake Sully y Ney'tiri han formado una familia y hacen todo lo posible por permanecer juntos. Sin embargo, dragon",
    "anio": 2010
  },
  {
    "titulo": "Pee Wee Herman's Big Adventure",
    "escritor": "Nidea",
    "actores": ["Denzel Washington" , "Morgan Freeman"],
    "franquicia": "Solo relleno",
    "sipnosis": "Lorem ipsum, Gandalf molestias nditiis ab! Bilbo",
    "anio": 2000
  },
  {
    "titulo": "Pulp Fiction",
    "escritor": "Roneo Santos",
    "actores": ["Debra Wilson","James Pitt","Woody Schultz"],
    "franquicia": "",
    "sipnosis": "Lorem ipsum, dolor seaque repudiandae modi ullam blanditiis ab! gold",
    "anio": 2000
  }

])


/*a) Obtener todos los documentos.*/
db.getCollection("movies").find({})

/*b) Obtener documentos con escritor igual a "Quentin Tarantino".*/
db.getCollection("movies").find({"escritor": "Quetin Tarantino"})

/*c) Obtener documentos con actores que incluyan a "Brad Pitt".*/
db.getCollection("movies").find({"actores": ["Brad Pitt","Julene Renee"]})

/*d) Obtener documentos con franquicia igual a "The Hobbit".*/
db.getCollection("movies").find({"franquicia": "The Hobbit"})

/*f) Obtener las pel�culas estrenadas entre el a�o 2000 y 2010.*/
db.getCollection("movies").find({"anio": {$gte:2000,$lte:2010}})

/*g) Agregar una actor llamado "Samuel L. Jackson" a la pel�cula "Pulp Fiction".*/
db.getCollection("movies").updateOne({"_id" : ObjectId("62b42087cd53abf28cb3781b")},
{$set:{"actores" : [ "Debra Wilson", "James Pitt", "Woody Schultz", "Samuel L.Jackson"]}})

/*h) Encontrar las pel�culas que en la sinopsis contengan la palabra "Bilbo".*/
db.getCollection("movies").find({"sipnosis": /.*Bilbo.*/});

/*i) Encontrar las pel�culas que en la sinopsis contengan la palabra "Gandalf".*/
db.getCollection("movies").find({"sipnosis": /.*Gandalf.*/});

/*j) Encontrar las pel�culas que en la sinopsis contengan la palabra "gold" y "dragon".*/
db.getCollection("movies").find({"sipnosis": /.*gold.*/});
db.getCollection("movies").find({"sipnosis": /.*dragon.*/});

/*k) Eliminar la pel�cula "Pee Wee Herman's Big Adventure" */
db.getCollection("movies").remove({"titulo": "Pee Wee Herman's Big Adventure"});

/*l) Eliminar la pel�cula "Avatar"*/
db.getCollection("movies").remove({"titulo": "Avatar"});
