db.getCollection("autor").insertMany([
  {
    "nombre": "Julio Cesar Herbas",
    "nombreUsuario": "julioblog16",
    "cuentaTwitter": "@Julio-Cesar",
    "descripcion": "Periodista internacional",
  },
  {
    "nombre": "Maria del Rosario Fuentes",
    "nombreUsuario": "mariaFuentes23",
    "cuentaTwitter": "@Rosario-Fuentes",
    "descripcion": "Reportera que trabaja por el bienestar de su gente",
  },
  {
    "nombre": "Juan Pablo Martinez Rocha",
    "nombreUsuario": "martinezJuan1900",
    "cuentaTwitter": "@Martinez_Juan",
    "descripcion": "Soy un blogguero",
  },
  {
    "nombre": "Juana Luisa Velasco",
    "nombreUsuario": "luisaJuana011",
    "cuentaTwitter": "@Luisa_Velasco",
    "descripcion": "Destacada periodista en el ambito ambiental",
  },
  {
    "nombre": "Lorenzo Garcia Pe�afiel",
    "nombreUsuario": "garciaLorenzo34",
    "cuentaTwitter": "@Garcia_Lorenzo1",
    "descripcion": "Reportero de la CNN",
  },
])
  
  db.getCollection("comentario").insertMany([
  {/*Comentario1 */
    "nombrePersona": "Fernanda de las Casas",
    "comentario": "Me parece muy bien que quieran cuidar la naturaleza",
    "fechaPublicacion": new Date(),
  },
  { /*Comentario3 */
    "nombrePersona": "Juan Luis Guerra",
    "comentario": "Que bien que se brinde cosas nuevas a la policia",
    "fechaPublicacion": new Date(),
  },
  {/*Comentario2 */
    "nombrePersona": "Alfonso Garcia",
    "comentario": "Los ni�os deben disfrutar de sus vacaciones, que bien",
    "fechaPublicacion": new Date(),
  },
  {/*Comentario4 */
    "nombrePersona": "Guadalupe Paredes Loaiza",
    "comentario": "Me alegra que al fin cuiden a los animales",
    "fechaPublicacion": new Date(),
  },
    {/*Comentario5 */
    "nombrePersona": "Arturo de la Barrera",
    "comentario": "Me parece perfecto que tomen conciencia del medio ambiente",
    "fechaPublicacion": new Date(),
  },
])
  
  var idJulio = ObjectId("62b4642055dcbe008e4b0edb")
var idMaria = ObjectId("62b4642055dcbe008e4b0edc")
var idJuan = ObjectId("62b4642055dcbe008e4b0edd")
var idJuana = ObjectId("62b4642055dcbe008e4b0ede")
var idLorenzo = ObjectId("62b4642055dcbe008e4b0edf")


var idComentario1 = ObjectId("62b46c1155dcbe008e4b0ee0")
var idComentario2 = ObjectId("62b46c1155dcbe008e4b0ee1")
var idComentario3 = ObjectId("62b4642055dcbe008e4b0edd")
var idComentario4 = ObjectId("62b46c1155dcbe008e4b0ee3")
var idComentario5 = ObjectId("62b46c1155dcbe008e4b0ee4")


db.getCollection("noticias").insertMany([
  {
    "titulo": "Hay multas por las fogatas y 250 funcionarios saldr�n a controlar",
    "cuerpo": "La Alcald�a advierte con sanciones a quienes incurran en el encendido de fuego en San Juan. La Gobernaci�n se�ala que la llamas pueden derivar en incendios",
    "fechaPublicacion": new Date(2022,6,23),
    "descripcion": "Los operativos se realizar�n esta noche a partir de las 21:00. Se movilizar�n a 250 funcionarios de las secretarias de Medio Ambiente, Seguridad Ciudadana y Abastecimiento para hacer el apagado de fogatas en los 15 distritos de la ciudad, en cumplimiento a la Ley Auton�mica 1184 que proh�be el encendido de fogatas.",
    "idAutor": idJuan,
    "idComentario": idComentario1
  },
  {
    "titulo": "Proh�ben dejar tareas a los escolares para las vacaciones de invierno",
    "cuerpo": "El receso pedag�gico inicia el pr�ximo 4 de julio",
    "fechaPublicacion": new Date(2022,6,22),
    "descripcion": "A solo d�as de iniciar el receso pedag�gico a nivel nacional, el ministro de Educaci�n, advirti� que queda prohibido dejar tareas a los escolares. Es m�s, aquellos docentes que lo hagan ser�n sancionados. ",
    "idAutor": idJuana,
    "idComentario": idComentario2
  },
  {
    "titulo": "Polic�a recibe veh�culos y motos para seguridad",
    "cuerpo": "La entrega del equipamiento a la Polic�a, este 22 de junio. ALCALD�A DE COCHABAMBA",
    "fechaPublicacion": new Date(2022,6,22),
    "descripcion": "El Comando Departamental de Polic�a de Cochabamba recibi� ayer veh�culos, material de escritorio y de bioseguridad que represent� un costo de m�s de 1,8 millones de bolivianos.",
    "idAutor": idMaria,
    "idComentario": idComentario3
  },
  {
    "titulo": "Plan busca salvar a paraba frente roja de la extinci�n",
    "cuerpo": "Entre las metas est� establecer un �rea protegida subnacional en la cuenca del r�o Pilcomayo para cuidar esta subpoblaci�n desprotegida.",
    "fechaPublicacion": new Date(2022,6,21),
    "descripcion": "Las metas son implementar acciones para evitar la disminuci�n de la poblaci�n de estas aves, evitar su tr�fico para comercializaci�n ilegal y que sean consideradas como una plaga, entre otras.",
    "idAutor": idLorenzo,
    "idComentario": idComentario4
  },
  {
    "titulo": "Calidad del aire es �regular�; piden no encender fogatas",
    "cuerpo": "Hoy en la noche se celebra la fiesta de San Juan. Desde la Alcald�a indicaron que realizar�n controles para evitar la contaminaci�n.",
    "fechaPublicacion": new Date(2022,6,22),
    "descripcion": "A horas de celebrarse la fiesta de San Juan hoy 23 de junio en la denominada �noche m�s fr�a del a�o�, la calidad del aire que respiramos en la ciudad de Cochabamba es regular, de acuerdo a datos de la Red de Monitoreo de la Calidad del Aire.",
    "idAutor": idJulio,
    "idComentario": idComentario5
  },
])
  /*e) Se deber� aplicar indexaci�n para mayor fluidez al manejo de datos.*/
db.getCollection('autor').createIndex({"nombre":1})
db.getCollection('autor').createIndex({"nombreUsuario":1})
db.getCollection('autor').getIndexes()


db.getCollection('comentario').createIndex({"nombrePersona":1})
db.getCollection('comentario').createIndex({"fechaPublicacion":1})
db.getCollection('comentario').getIndexes()

db.getCollection('noticias').createIndex({"titulo":1})
db.getCollection('noticias').createIndex({"fechaPublicacion":1})
db.getCollection('noticias').getIndexes()

  /*f) Realizar una consulta para cada colecci�n.*/
  db.getCollection('autor').find({})
  db.getCollection('comentario').find({})
  db.getCollection('noticias').find({})
  
  /*g) Verificar que un autor p�blico una noticia.*/
   db.getCollection("noticias").find({})
    db.getCollection("autor").find({})
    var idJulio = ObjectId("62b4642055dcbe008e4b0edb")
    db.getCollection("noticias").find({"idAutor": idJulio})
