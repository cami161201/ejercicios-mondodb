db.getCollection("Vehiculo").insertMany([
  {
    "Marca": "Suzuki",
    "Modelo": "Across",
    "Precio": 55000,
    "Colores": ["Gris","Verde","Azul","Rojo"]
  },
  {
    "Marca": "Toyota",
    "Modelo": "Auris",
    "Precio": 25980,
    "Colores":["Azul","Blanco","Verde","Negro","Rojo"]
  },
  {
    "Marca": "Ford",
    "Modelo": "B-Max",
    "Precio": 15400,
    "Colores": ["Gris","Verde","Negro","Rojo"]
  },
  {
    "Marca": "Volkswagen",
    "Modelo": "Amarok",
    "Precio": 35565,
    "Colores": ["Azul","Gris","Blanco","Verde","Negro","Rojo"]
  },
  {
    "Marca": "Ford",
    "Modelo": "EcoSport",
    "Precio": 19835,
    "Colores": ["Azul","Verde","Negro","Rojo"]
  },
  {
    "Marca": "Hyundai",
    "Modelo": "Bayon",
    "Precio": 19890,
    "Colores":  ["Azul","Gris","Negro"]
  },
  {
    "Marca": "Lexus",
    "Modelo": "CT",
    "Precio": 28700,
    "Colores": ["Azul","Gris","Blanco","Rojo"]
  },
  {
    "Marca": "Suzuki",
    "Modelo": "Ignis",
    "Precio": 15225,
    "Colores": ["Verde","Azul","Rojo"]
  }
])
  
db.getCollection("Vehiculo").count()
db.getCollection("Vehiculo").find({"Marca":"Ford"})
db.getCollection("Vehiculo").find({"Marca":"Suzuki"})
db.getCollection('Vehiculo').find({})
